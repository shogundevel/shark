###############################################################################
### Copyright ##################################################################
## 
## Copyright 2022 Daniel Alvarez <shogundevel@gmail.com>
## 
## Permission is hereby granted, free of charge, to any person
## obtaining a copy of this software and associated documentation files
## (the "Software"), to deal in the Software without restriction,
## including without limitation the rights to use, copy, modify, merge,
## publish, distribute, sublicense, and/or sell copies of the Software,
## and to permit persons to whom the Software is furnished to do so,
## subject to the following conditions:
## 
## The above copyright notice and this permission notice shall be
## included in all copies or substantial portions of the Software.
## 
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
## EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
## MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
## IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
## CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
## TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
## SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
## 
################################################################################

var Font
var FontLoad = Font.load
var FontFree = Font.unload
var FontSetSize = Font.setPixelSizes

var font_data = [ ]

function collect()
    var new_font_data = [ ]
    for font in font_data do
        if not font.unload() then
            new_font_data << font
    font_data = new_font_data

function exit()
    for font in font_data do
        font.font_rl = false
        font.unload()

class font
    function init(filename, size, color)
        self.filename = filename
        self.size = size
        self.color = color
        self.font = null
        self.font_rl = false
        self.reload()
    
    function reload()
        if self.font == null then
            self.font = FontLoad(self.filename)
            FontSetSize(self.font, self.size)
            font_data << self
        self.font_rl = true
    
    function unload()
        if self.font_rl then
            self.font_rl = false
            return false
        else
            if self.font != null then
                FontFree(self.font)
            self.font = null
            return true
    
    function get_height()
        return self.size
    
    function get_width(data)
        return 0
