################################################################################
### Copyright ##################################################################
## 
## Copyright 2022 Daniel Alvarez <shogundevel@gmail.com>
## 
## Permission is hereby granted, free of charge, to any person
## obtaining a copy of this software and associated documentation files
## (the "Software"), to deal in the Software without restriction,
## including without limitation the rights to use, copy, modify, merge,
## publish, distribute, sublicense, and/or sell copies of the Software,
## and to permit persons to whom the Software is furnished to do so,
## subject to the following conditions:
## 
## The above copyright notice and this permission notice shall be
## included in all copies or substantial portions of the Software.
## 
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
## EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
## MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
## IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
## CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
## TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
## SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
## 
################################################################################

import system.io: printf, puts
import system.path: get_base, get_tail, get_ext, remove_ext, join, unlink
import system.util: slice, extend
import system.string: format

import sharkc
import sharklink
import sharkemu
import sharkenv
import sharkmake
import sharkzip
import sharkunzip

import sharkenv.builtin: install, clone

function main(args)
    if sizeof(args) < 2 then
        printf("usage: % <command> <args>\n", [args[0]])
        puts("where <command> may be any of the following:\n")
        puts("\tplay\n")
        puts("\trun <filename> <args>\n")
        puts("\tcompile <target> <filename> <out>\n")
        puts("\tlink <target> <source> <main> <out> <libs>\n")
        puts("\tbuild <filename> <out> <libs>\n")
        puts("\tmake [action]\n")
        puts("\tzip <source> <target>\n")
        puts("\tunzip <source> <target>\n")
        puts("\tinstall <filename>\n")
        puts("\tclone <source> <target>\n")
        puts("(no command specified, now entering sharkenv)\n")
        sharkenv::main([args[0]])
        return
    var filename = args[0]
    var base = get_base(args[0])
    var command = args[1]
    args = slice(args, 2, sizeof(args))
    if command == "play" then
        sharkemu::play()
    else if command == "run" then
        args[0] << format("% %", [filename, command])
        sharkemu::main(args)
    else if command == "compile" then
        args[0] << format("% %", [filename, command])
        sharkc::main(args)
    else if command == "link" then
        args[0] << format("% %", [filename, command])
        sharklink::main(args)
    else if command == "make" then
        args[0] << format("% %", [filename, command])
        sharkmake::main(args)
    else if command == "zip" then
        args[0] << format("% %", [filename, command])
        sharkzip::main(args)
    else if command == "unzip" then
        args[0] << format("% %", [filename, command])
        sharkunzip::main(args)
    else if command == "install" then
        args[0] << format("% %", [filename, command])
        install(args)
    else if command == "clone" then
        args[0] << format("% %", [filename, command])
        clone(args)
    else if command == "build" then
        if sizeof(args) < 2 then
            printf("usage: % build <filename> <out> <libs>\n", [filename])
            puts("\tA shortcut that builds a shark archive (.shar) by calling the compiler on <filename>, links the output against <libs> and stores the result at <out>.\n")
            return
        var source = args[0]
        var out = args[1]
        var libs = slice(args, 2, sizeof(args))
        var bin = join(base, "out.bin")
        sharkc::main([filename, "c", source, bin])
        var link_args = [filename, "c", bin, remove_ext(get_tail(source)), out]
        extend(link_args, libs)
        sharklink::main(link_args)
        unlink(bin)
    else
        printf("unknown command '%'.", [command])
