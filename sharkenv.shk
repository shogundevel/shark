################################################################################
### Copyright ##################################################################
## 
## Copyright 2022 Daniel Alvarez <shogundevel@gmail.com>
## 
## Permission is hereby granted, free of charge, to any person
## obtaining a copy of this software and associated documentation files
## (the "Software"), to deal in the Software without restriction,
## including without limitation the rights to use, copy, modify, merge,
## publish, distribute, sublicense, and/or sell copies of the Software,
## and to permit persons to whom the Software is furnished to do so,
## subject to the following conditions:
## 
## The above copyright notice and this permission notice shall be
## included in all copies or substantial portions of the Software.
## 
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
## EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
## MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
## IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
## CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
## TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
## SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
## 
################################################################################

import system.io: puts, printf, read_line
import sharkenv.core

import sharkc
import sharklink
import sharkemu
import sharkmake
import sharkunzip
import sharkzip

function play(args)
    sharkemu::play()

core::command_table["compile"] = sharkc::main
core::command_table["link"] = sharklink::main
core::command_table["run"] = sharkemu::main
core::command_table["play"] = play
core::command_table["make"] = sharkmake::main
core::command_table["unzip"] = sharkunzip::main
core::command_table["zip"] = sharkzip::main

function main(args)
    if sizeof(args) != 1 then
        puts("usage: sharkenv\n")
        puts("\tUse this command without arguments to enter sharkenv, the shark command line environment.")
        return
    puts("Welcome to sharkenv, the shark command line environment. Type 'exit' to quit your sharkenv session.\n")
    while true do
        puts("> ")
        core::exec(read_line())
